// Require.js Configurations
// -------------------------
require.config({

    // Sets the js folder as the base directory for all future relative paths
    baseUrl: "./js/",

    waitSeconds: 200,

    // 3rd party script alias names (Easier to type "jquery" than "libs/jquery, etc")
    // probably a good idea to keep version numbers in the file names for updates checking
    paths: {

        // Core Libraries
        // --------------
        jquery: "../vendor/jquery/dist/jquery.min",
        jqueryMd5: "../vendor/jQuery-MD5/jquery.md5",
        foundation: "../vendor/foundation/js/foundation.min",
        modernizr: "../vendor/modernizr/modernizr",
        message: "../vendor/growl/javascripts/jquery.growl",
        json2: "../vendor/json2/json2",
        jstorage: "../vendor/jstorage/jstorage.min",
        prettyDate: "../vendor/jquery-prettydate/jquery.prettydate",
        emoticons: "../vendor/jQuery-CSSEmoticons/javascripts/jquery.cssemoticons.min",

        /* TESTS */
        specs: "../tests/spec/",

        /* APP */
        tools: "app/tools",
        formUtils: "app/formUtils",
        i18n: "app/i18n/i18n",
        social: "app/socialLoginApi",
        position: "app/position"
    },

    // Sets the configuration for your third party scripts that are not AMD compatible
    shim: {
        modernizr: {
            deps: ['jquery']
        },
        foundation: {
            deps: ['jquery', 'modernizr']
        },
        json2: {
            deps: ['jquery']
        },
        jstorage: {
            deps: ['json2']
        },
        jqueryMd5: {
            deps: ['jquery']
        },
        emoticons: {
            deps: ['jquery']
        },
        jasmine: {
            exports: 'jasmine'
        },
        'jasmine-html': {
            deps: ['jasmine'],
            exports: 'jasmine'
        },
        chess: {
            exports: 'Chess'
        }
    }
});

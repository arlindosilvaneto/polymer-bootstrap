window.social = {
    facebook: {
        public: '200994363431190'
    },
    google: {
        clientId: '930045482158-2e6278ut6vlioghslcd6bf8cri94s85n.apps.googleusercontent.com',
        public: 'AIzaSyAU57QDf8s7CBVKKr0w5Acjk-EAILu-w5k'
    }
};

window.fbAsyncInit = function() {
    // initialize app options
    FB.init({
        appId      : window.social.facebook.public,
        status     : false, // check login status
        cookie     : true, // enable cookies to allow the server to access the session
        xfbml      : true  // parse XFBML
    });

    // subscribe Facebook events
    FB.Event.subscribe('auth.authResponseChange', function(response) {
        //if (response.status === 'connected') {
            //loginFacebook();
        //} else if (response.status === 'not_authorized') {
            //FB.login();
        //} else {
            //FB.login();
        //}
    });
};

// load Facebook dependencies
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

// load Google login dependencies
(function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/client:plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();

// load Google maps dependencies
(function() {
    var hasGeolocation = navigator.geolocation,
        po = document.createElement('script'),
        scriptTags;

    po.type = 'text/javascript';
    po.async = true;
    if(hasGeolocation) {
        po.src = 'https://maps.googleapis.com/maps/api/js?key=' + window.social.google.public + '&sensor=true&libraries=places&callback=onMapLoad';
    } else {
        po.src = 'https://maps.googleapis.com/maps/api/js?key=' + window.social.google.public + '&sensor=false&libraries=places&callback=onMapLoad';
    }

    scriptTags = document.getElementsByTagName('script')[0];
    scriptTags.parentNode.insertBefore(po, scriptTags);

    window.onMapLoad = function() {
        console.log('Google Maps Loaded!');
    }
})();
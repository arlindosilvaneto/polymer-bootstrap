define(["app/i18n/messages", "tools"], function(messages, tools) {

    var getValue = function( key, data ) {
        var keyArray = key.split( "." );

        if ( keyArray.length === 1 ) {
            return data[ key ];
        } else {
            return getValue( keyArray.slice( 1 ).join( "." ), data[ keyArray[ 0 ] ] );
        }
    }

    var i = function(key) {
        var language = tools.language();

        return getValue(key, messages[language]);
    };

    return {
        i: i
    };
});
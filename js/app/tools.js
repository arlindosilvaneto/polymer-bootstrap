define(['jquery', "jstorage", "message"], function($) {
    var setup = {
        headers: {}
    };

    var scrollTo = function (id, speed) {
        speed = speed || 1000;

        $('html, body').animate({
            scrollTop: $(id).offset().top
        }, speed);
    };

    var setHeader = function(headerOptions) {
        var header;

        for(header in headerOptions) {
            setup.headers[header] = headerOptions[header];
        }

        $.ajaxSetup(setup);
    };

    var language = function(lang) {
        if(lang) {
            $.jStorage.set("se.language", lang);
        } else {
            return $.jStorage.get("se.language", "default");
        }
    };

    var showError = function(errorMessage, title) {
        title = title || 'Error';

        $.growl.error({title: title, message: errorMessage});
        //$.notify(errorMessage, {className: 'error', position: 'top center'});
    };

    var showMessage =  function(message, title) {
        title = title || "Success";

        $.growl.notice({title: title, message: message});
        //$.notify(message, {className: 'success', position: 'top center'});
    };

    var localFetch = function(key) {
        var value = '';
        
        if(key) {
            value = $.jStorage.get(key, '');
        }

        return value;
    };

    var localSave = function(key, value) {
        if(key) {
            $.jStorage.set(key, value);
        }
    };

    var localRemove = function(key) {
        if(key) {
            $.jStorage.deleteKey(key);
        }
    };

    var normalizeString = function(value) {
        return value.
            replace(/[áàâã]/g,'a').
            replace(/[éèê]/g,'e').
            replace(/[óòôõ]/g,'o').
            replace(/[úùûü]/g,'u').
            replace(/ç/g, 'c');
    };

    return {
        scrollTo: scrollTo,
        showError: showError,
        showMessage: showMessage,
        setHeader: setHeader,
        language: language,
        localFetch: localFetch,
        localSave: localSave,
        localRemove: localRemove,
        normalizeString: normalizeString
    };
});
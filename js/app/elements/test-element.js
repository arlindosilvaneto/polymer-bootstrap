define(['jquery', 'tools'], function($, tools) {
    var init,
        model,
        readyHandler;

    init = function() {
        Polymer('test-element', model);
    };

    readyHandler = function() {
        tools.showMessage('loaded');
    };

    model = {
        name: '',
        ready: readyHandler
    };

    return {
        init: init
    };
});
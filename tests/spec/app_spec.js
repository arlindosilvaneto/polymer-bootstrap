describe("App", function() {
    var _this = this;
    var flag = false;

    beforeEach(function() {
        runs(function() {
            require(['app/router'], function(router) {
                _this.router = router;

                flag = true;
            });
        });

        waitsFor(function() {
            return flag;
        });
    });

    afterEach(function() {
        flag = false;
    });

    it("app router should exists", function() {
        expect(_this.router).not.toBe(undefined);
    });
});

